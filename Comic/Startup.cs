﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Comic.Startup))]
namespace Comic
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
